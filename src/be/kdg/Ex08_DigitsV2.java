package be.kdg;

import java.util.Scanner;
public class Ex08_DigitsV2 {
    public static void main(String[] args) {
        final int MINIMUM = 1000;
        final int MAXIMUM = 9999;
        Scanner keyboard = new Scanner(System.in);
        int number;
        int sum = 0;
        System.out.print("Enter a 4-digit whole number (1000...): ");
        number = keyboard.nextInt();
        if (number < MINIMUM || MAXIMUM < number) {
            System.out.println("It should be a 4-digit number!");
        } else {
            sum += number % 10;
            number = number / 10;
            sum += number % 10;
            number = number / 10;
            sum += number % 10;
            number = number / 10;
            sum += number % 10;

            System.out.println("The sum of the digits of this number is " + sum);
        }
    }
}

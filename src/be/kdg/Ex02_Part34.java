package be.kdg;

public class Ex02_Part34 {
    public static void main(String[] args) {
        // Part 3
        int first = 8;
        int second = 5;

        System.out.println("+ :" + (first + second));
        System.out.println("- :" + (first - second));
        System.out.println("* :" + (first * second));
        System.out.println("/ :" + (first / second));
        System.out.println("% :" + (first % second));

        // division with double (floating point)
        // double divided = (double) first / (double) second;
        // System.out.println("Division: " + divided);

        // Part 4
        int result;
        result = ++first;
        System.out.println(result);
        result = first++;
        System.out.println(result);
        result = --second;
        System.out.println(result);
        result = second--;
        System.out.println(result);
    }
}

package be.kdg;

import java.util.Scanner;
public class Ex10_Switches {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        boolean switch1;
        boolean switch2;
        boolean switch3;

        System.out.print("Enter the state of switch 1 (true or false): ");
        switch1 = keyboard.nextBoolean();
        System.out.print("Enter the state of switch 2 (true or false): ");
        switch2 = keyboard.nextBoolean();
        System.out.print("Enter the state of switch 3 (true or false): ");
        switch3 = keyboard.nextBoolean();

        // Option 1:
        if (switch1 && switch2 || switch1 && switch3 || switch2 && switch3) {
            System.out.println("At least two switches are turned on.");
        }
        if ((switch1 && switch2 || switch1 && switch3 || switch2 && switch3)
                && (!switch1 || !switch2 || !switch3)) {
            System.out.println("Exactly two switches are turned on.");
        }
        if (!switch1 && !switch2 || !switch1 && !switch3 || !switch2 && !switch3) {
            System.out.println("At most one switch is turned on.");
        }
        System.out.println("-------");

        // An alternative approach, but focus was on && en ||
        int cnt = 0;
        if (switch1) cnt++;
        if (switch2) cnt++;
        if (switch3) cnt++;

        if (2 <= cnt) System.out.println("At least two switches are turned on.");
        if (2 == cnt) System.out.println("Exactly two switches are turned on.");
        if (1 <= cnt) System.out.println("At most one switch is turned on.");
        System.out.println("-------");

    }
}

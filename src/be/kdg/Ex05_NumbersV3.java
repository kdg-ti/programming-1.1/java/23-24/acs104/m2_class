package be.kdg;

import java.util.Scanner;
public class Ex05_NumbersV3 {
    public static void main(String[] args) {
        final long MINIMUM_DIVIDEND = 1_000_000_000_000L;
        final long MINIMUM_DIVISOR = 10_000_000L;
        long dividend;
        long divisor;
        double quotient;

        Scanner keyboard = new Scanner(System.in);

        System.out.print("Enter a 13-digit whole number: ");
        dividend = keyboard.nextLong();
        if (dividend < MINIMUM_DIVIDEND) {
            System.out.println("This number is too small..");
        } else {
            System.out.print("Enter an 8-digit whole number: ");
            divisor = keyboard.nextLong();
            if (dividend < MINIMUM_DIVISOR) {
                System.out.println("This number is too small..");
            } else {
                quotient = (double) dividend / (double) divisor;
                System.out.println("The quotient is " + quotient);
                System.out.println("Without the fractional part it's " + (long) quotient);
            }
        }
    }
}

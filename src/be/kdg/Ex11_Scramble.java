package be.kdg;

import java.util.Random;
import java.util.Scanner;
public class Ex11_Scramble {
    public static void main(String[] args) {
        String name;
        String nameTmp;
        String scramble ;
        int len;

        Scanner keyboard = new Scanner(System.in);
        Random random = new Random();

        System.out.print("Enter your name: ");
        name = keyboard.nextLine();

        nameTmp = new String(name);
        len = nameTmp.length();
        scramble = "";
        while(0 < len) {
            int pos = random.nextInt(len);
            char c = nameTmp.charAt(pos);

            if (c != ' ') {
                scramble = scramble + c;
            }
            nameTmp = nameTmp.substring(0, pos) + nameTmp.substring(pos + 1);

            // Debug, trace
            System.out.println("------");
            System.out.println("pos     : " + pos);
            System.out.println("char    : " + c);
            System.out.println("scramble: " + scramble);
            System.out.println("name    : " + nameTmp);

            len = nameTmp.length();  // len = len - 1
        }

        System.out.printf("Hi %s, your scrambled name is %s", name, scramble);
    }
}

package be.kdg;

public class Ex02_Part12 {
    public static void main(String[] args) {
        // Part 1
        /*
        int i1 = 2_000_000_000;
        int i2 = 2_000_000_000;
        System.out.print(i1 + i2);
         */
        // Output is -294967296

        // Part 2
        long l1 = 2_000_000_000L;
        long l2 = 2_000_000_000L;
        int result = (int)(l1 + l2);
        System.out.print(result);
        // Output is -294967296
    }
}

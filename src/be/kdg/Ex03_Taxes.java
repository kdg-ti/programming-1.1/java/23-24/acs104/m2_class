package be.kdg;

import java.util.Scanner;
public class Ex03_Taxes {
    public static void main(String[] args) {
        float percentage;
        float amount;
        float vatAmount;
        int choice;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter the VAT percentage: ");
        percentage = keyboard.nextFloat();
        System.out.print("Enter an amount in €: ");
        amount = keyboard.nextFloat();
        System.out.print("Make a choice (1 = inclusive, 2 = exclusive): ");
        choice = keyboard.nextInt();

        // Approach 1
        if (choice == 1) {
            vatAmount = amount * percentage / (100 + percentage);
            System.out.print("€" + (amount - vatAmount) + " + " + percentage + " VAT = €" + amount);
        }
        if (choice == 2) {
            vatAmount = amount * percentage / 100;
            System.out.print("€" + amount + " + " + percentage + " VAT = €" + (amount + vatAmount));
        }
        if (choice < 1 || 2 < choice) {
            System.out.print("Invalid choice\n");
        }

        System.out.println(); // new line

        // Approach 2
        if (choice == 1) {
            vatAmount = amount * percentage / (100 + percentage);
            System.out.printf("€%.2f + %.2f%% VAT = €%.2f\n", amount - vatAmount, percentage, amount);
        } else if (choice == 2) {
            vatAmount = amount * percentage / 100;
            System.out.printf("€%.2f + %.2f%% VAT = €%.2f\n", amount, percentage, amount + vatAmount);
        } else {
            System.out.print("Invalid choice\n");
        }
    }
}

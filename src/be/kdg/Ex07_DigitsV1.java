package be.kdg;

import java.util.Scanner;

public class Ex07_DigitsV1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int d1, d2, d3, d4, number;

        System.out.println("Enter four digits (0..9).");
        System.out.print("The first digit: ");
        d1 = keyboard.nextInt();
        System.out.print("The second digit: ");
        d2 = keyboard.nextInt();
        System.out.print("The third digit: ");
        d3 = keyboard.nextInt();
        System.out.print("The fourth digit: ");
        d4 = keyboard.nextInt();
        if (d1 < 0 || d2 < 0 || d3 < 0 || d4 < 0 || d1 > 9 || d2 > 9 || d3 > 9 || d4 > 9) {
            System.out.println("Make sure each digit lies in the range [0-9]!");
        } else {
            number = d1 * 1000 + d2 * 100 + d3 * 10 + d4;
            System.out.println("The number is " + number);
        }

    }
}

package be.kdg;

import java.util.Scanner;
public class Ex04_NumbersV2 {
    public static void main(String[] args) {

        final int MIN_VALUE = 100_000;
        final int MAX_VALUE = 999_999;
        int first;
        int second;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Please enter 1st 6 digit number: ");
        first =keyboard.nextInt();
        System.out.print("Please enter 2nd 6 digit number: ");
        second =keyboard.nextInt();

        // Approach 1
        if((MIN_VALUE <=first)
                &&(first <=MAX_VALUE)
                &&(MIN_VALUE <=second)
                &&(second <=MAX_VALUE)) {
            long product = (long) first * (long) second;
            long lastDigits = product % MIN_VALUE;
            System.out.println("The product is " + product);
            System.out.println("The last 5 digits are " + lastDigits);
        } else {
            System.out.println("One of the numbers is too small.");
        }

        // Approach 2
        if((first<MIN_VALUE)||(MAX_VALUE<first)||(second<MIN_VALUE)||(MAX_VALUE<second)) {
            System.out.println("One of the numbers is too small.");
        } else     {
            long product = (long) first * second;
            System.out.println("The product is " + product);
            System.out.printf("The last 5 digits are %05d \n", product % MIN_VALUE);
        }
    }
}
